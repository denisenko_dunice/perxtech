import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from './table/table.component';
import { DivComponent } from './div/div.component';
import { MainComponent } from './main/main.component';


const routes: Routes = [

  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {path: 'main', component: MainComponent},
  {path: 'table', component: TableComponent, data: { animation: 'isTable' }},
  {path: 'div', component: DivComponent, data: { animation: 'isDiv' }}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
