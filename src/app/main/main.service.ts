import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  public router: Router;

  constructor( router: Router ) {
    this.router = router;
   }
}
