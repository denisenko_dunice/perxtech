import { Component, OnInit, ViewChild } from '@angular/core';
import example from '../../assets/example.json';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

export interface IBook {
  id: string;
  type: string;
  name: string;
  updated_at: string;
  links: string;
  relationships: string;
}

const BOOKS: IBook[] = example.data.map(item => ({
  id: item['id'],
  type: item['attributes']['display_properties']['type'],
  name: item['attributes']['content'],
  updated_at: item['attributes']['updated_at'],
  links: item['links']['self'],
  relationships: item['relationships']['authors']['links']['self'],
}));

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'type', 'updated_at', 'links', 'relationships'];
  dataSource = new MatTableDataSource(BOOKS);

  constructor() { }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

}
