import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import { AppMaterialModule } from '../app-material.module';
import { NavbarModule } from '../navbar/navbar.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [TableComponent],
  imports: [
    CommonModule,
    AppMaterialModule,
    NavbarModule,
    BrowserAnimationsModule
  ]
})
export class TableModule { }
