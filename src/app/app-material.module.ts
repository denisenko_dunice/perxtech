
import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatSelectModule,
  MatChipsModule, MatCardModule,
  MatIconModule, MatAutocompleteModule,
  MatInputModule, MatToolbarModule,
  MatCheckboxModule, MatDialogModule, MatTableModule, MatDividerModule } from "@angular/material";
  import { MatSortModule } from '@angular/material/sort';


@NgModule({
  declarations: [],
  imports: [
    MatButtonModule,
    MatChipsModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTableModule,
    MatSortModule,
    MatDividerModule
  ],
  exports: [
    MatButtonModule,
    MatChipsModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTableModule,
    MatSortModule,
    MatDividerModule
  ],
})
export class AppMaterialModule { }
