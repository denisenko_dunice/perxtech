import { Component } from '@angular/core';
import { NavbarService } from './navbar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  constructor(private navbarService: NavbarService) { }

  navigateToTable() {
    this.navbarService.router.navigate(['../table']);
  }
  navigateToDiv() {
    this.navbarService.router.navigate(['../div']);
  }
}
